﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	private float reloadTime = 1f;
	private float timer;

	void Start () {
		timer = reloadTime;
	}

	void Update () {
		// do not run if the game is paused
		if (Time.timeScale == 0) {
			return;
		}

		// increment timer
		timer += Time.deltaTime;

		// when the button is pushed and reload time is reached, fire a bullet
		if ((Input.GetButtonDown("Fire1")) && (timer >= reloadTime))  {
			
			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;


			// reset timer
			timer = 0;

		}
	}

}
