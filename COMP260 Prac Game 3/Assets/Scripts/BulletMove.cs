﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {
	
	// separate speed and direction so we can 
	// tune the speed without changing the code
	public float speed = 10.0f;
	public Vector3 direction;
	private Rigidbody rigidbody;
	private float timer;
	private float timeTillDeath = 5;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();    
	}

	void FixedUpdate () {
		timer += Time.deltaTime;
		rigidbody.velocity = speed * direction;

		// check if lifetime of bullet reached & destroy
		if (timer >= timeTillDeath) {
			Destroy (gameObject);

		}
	}

	void OnCollisionEnter(Collision collision) {
		// Destroy the bullet
		Destroy(gameObject);
	}


}
